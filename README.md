# Discovery Instance Module

This module is capable to register an instance as part of an existing CloudMap DNS Locator

Module Input Variables
----------------------

- `name` - general name
- `namespace_id` - namespace identifier
- `instance_id` - instance identifier
- `ip` - ip address to register

Usage
-----

```hcl
module "discovery_instance" {
  source        = "git::https://gitlab.com/mesabg-tfmodules/discovery-instance.git"

  name = {
    slug        = "name"
    full        = "Full_Name"
  }

  namespace_id  = "ns-xxxx"
  instance_id   = "i-xxxx"
  ip            = "10.0.0.5"
}
```

Outputs

 - `discovery_service` - Created Discovery Service
 - `discovery_instance` - Created Discovery Instance

Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
