terraform {
  required_version = ">= 0.13.2"
}

resource "aws_service_discovery_service" "discovery_service" {
  name                = var.name.slug

  dns_config {
    namespace_id      = var.namespace_id

    dns_records {
      ttl             = 60
      type            = "A"
    }

    routing_policy    = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}

resource "aws_cloudformation_stack" "discovery_instance" {
  name          = var.name.full
  on_failure    = "DELETE"
  template_body = <<TEMPLATE
  {
    "AWSTemplateFormatVersion": "2010-09-09",
    "Resources": {
      "ServiceDiscoveryInstance": {
        "Properties": {
          "InstanceAttributes": {
            "AWS_INSTANCE_IPV4": "${var.ip}"
          },
          "InstanceId": "${var.instance_id}",
          "ServiceId": "${aws_service_discovery_service.discovery_service.id}"
        },
        "Type": "AWS::ServiceDiscovery::Instance"
      }
    }
  }
  TEMPLATE
}
