output "discovery_service" {
  value       = aws_service_discovery_service.discovery_service
  description = "Created Discovery Service"
}

output "discovery_instance" {
  value       = aws_cloudformation_stack.discovery_instance
  description = "Created Discovery Instance"
}
