variable "name" {
  type = object({
    slug = string
    full = string
  })
  description = "General discovery instance name"
}

variable "namespace_id" {
  type        = string
  description = "CloudMap namespace identifier"
}

variable "instance_id" {
  type        = string
  description = "EC2 instance identifier"
}

variable "ip" {
  type        = string
  description = "EC2 instance ip"
}
